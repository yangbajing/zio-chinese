# ZIO 概述

- [摘要](summary.md)
- [创建 Effect](creating-effects.md)
- [基本操作](basic-operations.md)
- [处理错误](handling-errors.md)
- [处理资源](handling-resources.md)

